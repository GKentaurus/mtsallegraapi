/*
 * 2017 Metasysco
 *
 * AVISO DE LICENCIA
 *
 * Este módulo es de uso único y exclusivo del comprador y propietario
 * de la tienda de Prestashop en la cual está asociada la cuenta registrada
 * en http://addons.prestashop.com/
 *
 * Prohíbase la copia y distribución ilegal de este módulo.
 *
 * ADVERTENCIA
 *
 * No edite, modifique o altere el código de este archivo, si usted
 * tiene planeado a futuro actualizar la plataforma Prestashop a una
 * nueva versión (Aplicable para la versión de Prestashop 1.6.x.x).
 * Si usted desea modificar este módulo para su necesidad, por favor
 * contáctenos por medio del correo electrónico development@metasysco.com
 * o visite nuestra página web http://www.metasysco.com para mas información.
 *
 * @author Carlos Moreno <carlos.moreno@metasysco.com.co>
 * @copyright 2017 Metasysco S.A.S.
 * @license Commercial License
 * @category
 * @version
 */
$(document).ready(function () {
    var oldErrors = $('div.alert.alert-danger').html()
        .replace('passwd', 'contraseña')
        .replace('dirección1', 'dirección')
        .replace('siret', 'número de Documento')
        .replace('<b>ape</b>', '<b>tipo de Documento</b>')
        .replace('una Provincia o Estado', 'una Provincia, Estado o Departamento');
    $('div.alert.alert-danger').html(oldErrors);

    $('#center_column').change(function () {
        $('#siret').focusout(function () {
            var lengthSiret = $(this).val().length;
            var separatorPos = $(this).val().indexOf('-');
            var differencePos = lengthSiret - separatorPos;

            if ($('#ape').val() === 'NIT' && separatorPos > 0 && differencePos === 2) {
                $(this).parent().removeClass('form-error').addClass('form-ok');
            } else if ($('#ape').val() === 'NIT' && separatorPos < 0) {
                $(this).parent().removeClass('form-ok').addClass('form-error');
            } else if ($('#ape').val() !== 'NIT') {
                $(this).parent().removeClass('form-error').removeClass('form-ok');
            }
        });
    });
});
