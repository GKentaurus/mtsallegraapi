/*
 * 2017 Metasysco
 *
 * AVISO DE LICENCIA
 *
 * Este módulo es de uso único y exclusivo del comprador y propietario
 * de la tienda de Prestashop en la cual está asociada la cuenta registrada
 * en http://addons.prestashop.com/
 *
 * Prohíbase la copia y distribución ilegal de este módulo.
 *
 * ADVERTENCIA
 *
 * No edite, modifique o altere el código de este archivo, si usted
 * tiene planeado a futuro actualizar la plataforma Prestashop a una
 * nueva versión (Aplicable para la versión de Prestashop 1.6.x.x).
 * Si usted desea modificar este módulo para su necesidad, por favor
 * contáctenos por medio del correo electrónico development@metasysco.com
 * o visite nuestra página web http://www.metasysco.com para mas información.
 *
 * @author Carlos Moreno <carlos.moreno@metasysco.com.co>
 * @copyright 2017 Metasysco S.A.S.
 * @license Commercial License
 * @category
 * @version
 */
$(document).ready(function () {
    $(document).on('submit', '#create-account_form', function (e) {
        e.preventDefault();
        submitFunction();
    });
    $('.is_customer_param').hide();
});

function submitFunction() {
    $('#create_account_error').html('').hide();
    $.ajax({
        type: 'POST',
        url: baseUri + '?rand=' + new Date().getTime(),
        async: true,
        cache: false,
        dataType: "json",
        headers: {"cache-control": "no-cache"},
        data:
            {
                controller: 'authentication',
                SubmitCreate: 1,
                ajax: true,
                email_create: $('#email_create').val(),
                back: $('input[name=back]').val(),
                token: token
            },
        success: function (jsonData) {
            if (jsonData.hasError) {
                var errors = '';
                for (error in jsonData.errors)
                    //IE6 bug fix
                    if (error != 'indexOf')
                        errors += '<li>' + jsonData.errors[error] + '</li>';
                $('#create_account_error').html('<ol>' + errors + '</ol>').show();
            }
            else {
                // adding a div to display a transition
                $('#center_column').html('<div id="noSlide">' + $('#center_column').html() + '</div>');
                $('#noSlide').fadeOut('slow', function () {
                    $('#noSlide').html(jsonData.page);
                    $(this).fadeIn('slow', function () {
                        if (typeof bindUniform !== 'undefined')
                            bindUniform();
                        if (typeof bindStateInputAndUpdate !== 'undefined')
                            bindStateInputAndUpdate();
                        document.location = '#account-creation';
                    });
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            error = "TECHNICAL ERROR: unable to load form.\n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus;
            if (!!$.prototype.fancybox) {
                $.fancybox.open([
                        {
                            type: 'inline',
                            autoScale: true,
                            minHeight: 30,
                            content: "<p class='fancybox-error'>" + error + '</p>'
                        }],
                    {
                        padding: 0
                    });
            }
            else
                alert(error);
        }
    });
}