/*
 * 2017 Metasysco
 *
 * AVISO DE LICENCIA
 *
 * Este módulo es de uso único y exclusivo del comprador y propietario
 * de la tienda de Prestashop en la cual está asociada la cuenta registrada
 * en http://addons.prestashop.com/
 *
 * Prohíbase la copia y distribución ilegal de este módulo.
 *
 * ADVERTENCIA
 *
 * No edite, modifique o altere el código de este archivo, si usted
 * tiene planeado a futuro actualizar la plataforma Prestashop a una
 * nueva versión (Aplicable para la versión de Prestashop 1.6.x.x).
 * Si usted desea modificar este módulo para su necesidad, por favor
 * contáctenos por medio del correo electrónico development@metasysco.com
 * o visite nuestra página web http://www.metasysco.com para mas información.
 *
 * @author Carlos Moreno <carlos.moreno@metasysco.com.co>
 * @copyright 2017 Metasysco S.A.S.
 * @license Commercial License
 * @category
 * @version
 */
$(document).ready(function () {
    $('#left_column').remove();
    $('#right_column').remove();
    $('#center_column').removeClass('col-sm-9').addClass('col-sm-12');

    /**
     * contactCreate - create.tpl
     */
    var listFields = ['dni', 'alias', 'phone', 'phone_mobile', 'address', 'location'];

    $('.selectorProfile').change(function () {
        var selector = '#'.concat($(this).attr('id'));
        var header = selector.replace('_option', '_');

        listFields.forEach(function (item, index) {
            var completeID = header.concat(item);
            $(completeID).val($(selector).val());
        });
    });
    console.log('Putos todos');
    console.log($('div.alert.alert-danger').find('ape'));
});


